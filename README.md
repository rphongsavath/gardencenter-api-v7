
## Name
GardenCenter-API-V7

## Description
This REST API application allows full CRUD and custom request for Garden Center Store. Users will be able to make changes and queries to an establishing database. The API can serve data to customer facing mobile and web applications.

## Installation
For installation, please have the latest version of PostgresSQL installed and confirm the application.yml file in the resource folder is updated as followed:
```
spring:
  jpa:
    database: POSTGRESQL
    show-sql: true
    hibernate:
      ddl-auto: create-drop
  datasource:
    platform: postgres
    url: jdbc:postgresql://localhost:5432/postgres
    username: postgres
    password: postgres
    driverClassName: org.postgresql.Driver
```
## Running the application
1. Open the ServerRunner class in Intellij or equivalent platform and run the file. SpringBoot should run successfully and the data will populate the listing of entites located in the dataloader once the server starts. If there are any issues with the dependencies, please open the POM file and confirm the dependencies are updated.
2. Please import the postman collection via the link below and run the requests. The request listings can be autorun or individually selected and ran.

## Running Testing Package
1. Navigate to the java package in the test package as follows: src -> test -> java. 
2. Right click on the java package, hover to the More Run/Debug option, then select Run all Tests with Coverage
3. Navigate through the package until you see the Controllers Package and double click on the package.
4. Confirm the percent coverage in the ServiceImpl classes for Unit Testing
5. Confirm the percent coverage in the Controller classes for Java Integration Testing

## Postman Collection Link
https://orange-trinity-452585.postman.co/workspace/Postman-Collections~1c8eba04-51e1-4722-8e0d-a9a61a361daa/collection/25329122-56013485-390d-4ae6-bc8d-ad56aa76105c?action=share&creator=25329122

## Contributing
The project is open to contributions and you must be part of the Create Opportunity program.


## Authors and acknowledgment
Don Moore and Richy Phongsavath are authors of this project as part of the Crate Opportunity program at Central Piedmont Community College.

## License
This application is open sourced and stored in a public GitLab repository.

## Project status
The project is completed unless specifically stated by review. 